package com.org.tamimimarkets.presenter.genricpresenter;

import org.json.JSONObject;

import java.util.HashMap;

public interface GenericPresenterInterface {
    void getResponse(int requestType, String tag, String url, final JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog);
}
