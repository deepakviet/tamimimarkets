package com.org.tamimimarkets.presenter.registeraccountpresenter;

import org.json.JSONObject;

import java.util.HashMap;

public interface RegisterPresenterInterface {
    void getRegisterResponse(int requestType, String tag, String url, final JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog);
}
