package com.org.tamimimarkets.presenter.dashboardpresenter;

import com.org.tamimimarkets.pojo.DashBoardData;
import com.org.tamimimarkets.pojo.DashboardPojo;
import com.org.tamimimarkets.pojo.LoginPojo;

public interface DashBoardViewInterface {
    void getDashBoardResponse(DashboardPojo dashboardPojo);
    void onError(String response);
}
