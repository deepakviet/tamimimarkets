package com.org.tamimimarkets.presenter.genricpresenter;

import android.content.Context;

import com.google.gson.Gson;
import com.org.tamimimarkets.pojo.LoginPojo;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONObject;

import java.util.HashMap;

public class GenericPresenter implements GenericPresenterInterface, CustomJsonRequest.OnServerResponse {

    private String TAG = "GenericPresenter";
    private GenericViewInterface genericViewInterface;
    private CustomJsonRequest customJsonRequest;

    public GenericPresenter(Context context, GenericViewInterface genericViewInterface) {
        this.genericViewInterface = genericViewInterface;
        customJsonRequest = new CustomJsonRequest(context, this);
    }


    @Override
    public void getResponse(int requestType, String tag, String url, JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog) {
        this.TAG = tag;
        customJsonRequest.getDataFromServer(requestType,tag,url,jsonObject,headers,isCachedData,isProgressDialog);
    }

    @Override
    public void getJsonFromServer(boolean flag,String tag, String url, JSONObject jsonObject, String error) {
        if (flag && jsonObject != null) {
            genericViewInterface.getResponse(tag,jsonObject);
        } else {
            genericViewInterface.onError(error);
        }
    }

}
