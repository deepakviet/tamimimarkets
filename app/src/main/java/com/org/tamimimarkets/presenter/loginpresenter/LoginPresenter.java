package com.org.tamimimarkets.presenter.loginpresenter;

import android.content.Context;

import com.google.gson.Gson;
import com.org.tamimimarkets.pojo.LoginPojo;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONObject;

import java.util.HashMap;

public class LoginPresenter implements LoginPresenterInterface, CustomJsonRequest.OnServerResponse {

    private String TAG = "LoginPresenter";
    private LoginViewInterface loginViewInterface;
    private CustomJsonRequest customJsonRequest;

    public LoginPresenter(Context context, LoginViewInterface loginViewInterface) {
        this.loginViewInterface = loginViewInterface;
        customJsonRequest = new CustomJsonRequest(context, this);
    }


    @Override
    public void getLoginResponse(int requestType, String tag, String url, JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog) {
        this.TAG = tag;
        customJsonRequest.getDataFromServer(CustomJsonRequest.POST,tag,url,jsonObject,headers,isCachedData,isProgressDialog);
    }

    @Override
    public void getJsonFromServer(boolean flag,String tag,  String url, JSONObject jsonObject, String error) {
        if (flag && jsonObject != null) {
            parseConfig(jsonObject);
        } else {
            loginViewInterface.onError(error);
        }
    }


    private void parseConfig(JSONObject jsonObject) {
        Gson gson = new Gson();
        LoginPojo loginPojo = gson.fromJson(jsonObject.toString(), LoginPojo.class);
        loginViewInterface.getLoginResponse(loginPojo);
    }
}
