package com.org.tamimimarkets.presenter.loginpresenter;

import com.org.tamimimarkets.pojo.LoginPojo;

public interface LoginViewInterface {
    void getLoginResponse(LoginPojo loginPojo);
    void onError(String response);
}
