package com.org.tamimimarkets.presenter.dashboardpresenter;

import org.json.JSONObject;

import java.util.HashMap;

public interface DashBoardPresenterInterface {
    void getDashBoardResponse(int requestType, String tag, String url, final JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog);
}
