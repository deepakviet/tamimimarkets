package com.org.tamimimarkets.presenter.registeraccountpresenter;

import android.content.Context;

import com.google.gson.Gson;
import com.org.tamimimarkets.pojo.RegisterPojo;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONObject;

import java.util.HashMap;

public class RegisterPresenter implements RegisterPresenterInterface, CustomJsonRequest.OnServerResponse {

    private String TAG = "RegisterUserPresenter";
    private RegisterViewInterface registerViewInterface;
    private CustomJsonRequest customJsonRequest;

    public RegisterPresenter(Context context, RegisterViewInterface registerViewInterface) {
        this.registerViewInterface = registerViewInterface;
        customJsonRequest = new CustomJsonRequest(context, this);
    }


    @Override
    public void getRegisterResponse(int requestType, String tag, String url, JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog) {
        this.TAG = tag;
        customJsonRequest.getDataFromServer(CustomJsonRequest.POST,tag,url,jsonObject,headers,isCachedData,isProgressDialog);
    }

    @Override
    public void getJsonFromServer(boolean flag,String tag,  String url, JSONObject jsonObject, String error) {
        if (flag && jsonObject != null) {
            parseConfig(jsonObject);
        } else {
            registerViewInterface.onError(error);
        }
    }


    private void parseConfig(JSONObject jsonObject) {
        Gson gson = new Gson();
        RegisterPojo registerPojo = gson.fromJson(jsonObject.toString(), RegisterPojo.class);
        registerViewInterface.getRegisterResponse(registerPojo);
    }

}
