package com.org.tamimimarkets.presenter.storelistpresenter;

import android.content.Context;

import com.google.gson.Gson;
import com.org.tamimimarkets.pojo.LoginPojo;
import com.org.tamimimarkets.pojo.StoreListPojo;
import com.org.tamimimarkets.presenter.loginpresenter.LoginPresenterInterface;
import com.org.tamimimarkets.presenter.loginpresenter.LoginViewInterface;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONObject;

import java.util.HashMap;

public class StoreListPresenter implements StoreListPresenterInterface, CustomJsonRequest.OnServerResponse {

    private String TAG = "LoginPresenter";
    private StoreListViewInterface storeListViewInterface;
    private CustomJsonRequest customJsonRequest;

    public StoreListPresenter(Context context, StoreListViewInterface storeListViewInterface) {
        this.storeListViewInterface = storeListViewInterface;
        customJsonRequest = new CustomJsonRequest(context, this);
    }


    @Override
    public void getStoreListResponse(int requestType, String tag, String url, JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog) {
        this.TAG = tag;
        customJsonRequest.getDataFromServer(requestType,tag,url,jsonObject,headers,isCachedData,isProgressDialog);
    }

    @Override
    public void getJsonFromServer(boolean flag,String tag,  String url, JSONObject jsonObject, String error) {
        if (flag && jsonObject != null) {
            parseConfig(jsonObject);
        } else {
            storeListViewInterface.onError(error);
        }
    }


    private void parseConfig(JSONObject jsonObject) {
        Gson gson = new Gson();
        StoreListPojo storeListPojo = gson.fromJson(jsonObject.toString(), StoreListPojo.class);
        storeListViewInterface.getStoreListResponse(storeListPojo);
    }
}
