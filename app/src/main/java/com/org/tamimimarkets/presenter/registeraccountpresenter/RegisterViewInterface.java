package com.org.tamimimarkets.presenter.registeraccountpresenter;

import com.org.tamimimarkets.pojo.LoginPojo;
import com.org.tamimimarkets.pojo.RegisterPojo;

public interface RegisterViewInterface {
    void getRegisterResponse(RegisterPojo registerPojo);
    void onError(String response);
}
