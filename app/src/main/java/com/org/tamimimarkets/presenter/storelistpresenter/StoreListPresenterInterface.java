package com.org.tamimimarkets.presenter.storelistpresenter;

import org.json.JSONObject;

import java.util.HashMap;

public interface StoreListPresenterInterface {
    void getStoreListResponse(int requestType, String tag, String url, final JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog);
}
