package com.org.tamimimarkets.presenter.dashboardpresenter;

import android.content.Context;

import com.google.gson.Gson;
import com.org.tamimimarkets.pojo.DashboardPojo;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONObject;

import java.util.HashMap;

public class DashBoardPresenter implements DashBoardPresenterInterface, CustomJsonRequest.OnServerResponse {

    private String TAG = "DashBoardPresenter";
    private DashBoardViewInterface dashBoardViewInterface;
    private CustomJsonRequest customJsonRequest;

    public DashBoardPresenter(Context context, DashBoardViewInterface dashBoardViewInterface) {
        this.dashBoardViewInterface = dashBoardViewInterface;
        customJsonRequest = new CustomJsonRequest(context, this);
    }

    @Override
    public void getDashBoardResponse(int requestType, String tag, String url, JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog) {
        this.TAG = tag;
        customJsonRequest.getDataFromServer(requestType,tag,url,jsonObject,headers,isCachedData,isProgressDialog);
    }

    @Override
    public void getJsonFromServer(boolean flag,String tag, String url, JSONObject jsonObject, String error) {
        if (flag && jsonObject != null) {
            parseConfig(jsonObject);
        } else {
            dashBoardViewInterface.onError(error);
        }
    }


    private void parseConfig(JSONObject jsonObject) {
        Gson gson = new Gson();
        DashboardPojo dashboardPojo = gson.fromJson(jsonObject.toString(), DashboardPojo.class);
        dashBoardViewInterface.getDashBoardResponse(dashboardPojo);
    }


}
