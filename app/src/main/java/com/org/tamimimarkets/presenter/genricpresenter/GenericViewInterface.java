package com.org.tamimimarkets.presenter.genricpresenter;

import org.json.JSONObject;

public interface GenericViewInterface {
    void getResponse(String tag,JSONObject jsonObject);
    void onError(String response);
}
