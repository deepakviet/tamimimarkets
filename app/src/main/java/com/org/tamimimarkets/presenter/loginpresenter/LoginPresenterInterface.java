package com.org.tamimimarkets.presenter.loginpresenter;

import org.json.JSONObject;

import java.util.HashMap;

public interface LoginPresenterInterface {
    void getLoginResponse(int requestType, String tag, String url, final JSONObject jsonObject, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog);
}
