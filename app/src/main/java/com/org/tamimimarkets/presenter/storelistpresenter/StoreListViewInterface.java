package com.org.tamimimarkets.presenter.storelistpresenter;

import com.org.tamimimarkets.pojo.LoginPojo;
import com.org.tamimimarkets.pojo.StoreListPojo;

public interface StoreListViewInterface {
    void getStoreListResponse(StoreListPojo storeListPojo);
    void onError(String response);
}
