package com.org.tamimimarkets.presenter.forgetpasswordpresenter;

import com.org.tamimimarkets.pojo.LoginPojo;

public interface ForgetPasswordViewInterface {
    void getLoginResponse(LoginPojo loginPojo);
    void onError(String response);
}
