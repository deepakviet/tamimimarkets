package com.org.tamimimarkets.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderData {

    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("collectOrder")
    @Expose
    private CollectOrder collectOrder;
    @SerializedName("discounts")
    @Expose
    private List<Object> discounts = null;
    @SerializedName("taxesIncluded")
    @Expose
    private boolean taxesIncluded;
    @SerializedName("items")
    @Expose
    private List<Object> items = null;
    @SerializedName("shippings")
    @Expose
    private List<Object> shippings = null;
    @SerializedName("refunds")
    @Expose
    private List<Object> refunds = null;
    @SerializedName("order_status")
    @Expose
    private String order_status;
    @SerializedName("isTestOrder")
    @Expose
    private boolean isTestOrder;
    @SerializedName("fulfillmentStatus")
    @Expose
    private String fulfillmentStatus;
    @SerializedName("customerId")
    @Expose
    private CustomerId customerId;
    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;
    @SerializedName("productId")
    @Expose
    private List<ProductId> productId = null;
    @SerializedName("subtotalPrice")
    @Expose
    private Object subtotalPrice;
    @SerializedName("totalDiscount")
    @Expose
    private Object totalDiscount;
    @SerializedName("totalPrice")
    @Expose
    private Object totalPrice;
    @SerializedName("totalWeight")
    @Expose
    private String totalWeight;
    @SerializedName("lastModified")
    @Expose
    private String lastModified;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private int __v;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CollectOrder getCollectOrder() {
        return collectOrder;
    }

    public void setCollectOrder(CollectOrder collectOrder) {
        this.collectOrder = collectOrder;
    }

    public List<Object> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Object> discounts) {
        this.discounts = discounts;
    }

    public boolean isTaxesIncluded() {
        return taxesIncluded;
    }

    public void setTaxesIncluded(boolean taxesIncluded) {
        this.taxesIncluded = taxesIncluded;
    }

    public List<Object> getItems() {
        return items;
    }

    public void setItems(List<Object> items) {
        this.items = items;
    }

    public List<Object> getShippings() {
        return shippings;
    }

    public void setShippings(List<Object> shippings) {
        this.shippings = shippings;
    }

    public List<Object> getRefunds() {
        return refunds;
    }

    public void setRefunds(List<Object> refunds) {
        this.refunds = refunds;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public boolean isIsTestOrder() {
        return isTestOrder;
    }

    public void setIsTestOrder(boolean isTestOrder) {
        this.isTestOrder = isTestOrder;
    }

    public String getFulfillmentStatus() {
        return fulfillmentStatus;
    }

    public void setFulfillmentStatus(String fulfillmentStatus) {
        this.fulfillmentStatus = fulfillmentStatus;
    }

    public CustomerId getCustomerId() {
        return customerId;
    }

    public void setCustomerId(CustomerId customerId) {
        this.customerId = customerId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<ProductId> getProductId() {
        return productId;
    }

    public void setProductId(List<ProductId> productId) {
        this.productId = productId;
    }

    public Object getSubtotalPrice() {
        return subtotalPrice;
    }

    public void setSubtotalPrice(Object subtotalPrice) {
        this.subtotalPrice = subtotalPrice;
    }

    public Object getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Object totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Object getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Object totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

}
