package com.org.tamimimarkets.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeftNavData {

    @SerializedName("isSubCategory")
    @Expose
    private boolean isSubCategory;
    @SerializedName("parentCategoryId")
    @Expose
    private String parentCategoryId;
    @SerializedName("subCategory")
    @Expose
    private List<Object> subCategory = null;
    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("categoryLevel")
    @Expose
    private int categoryLevel;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private int __v;
    @SerializedName("iconImageUrl")
    @Expose
    private String iconImageUrl;
    @SerializedName("id")
    @Expose
    private String id;

    public boolean isIsSubCategory() {
        return isSubCategory;
    }

    public void setIsSubCategory(boolean isSubCategory) {
        this.isSubCategory = isSubCategory;
    }

    public String getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(String parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public List<Object> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<Object> subCategory) {
        this.subCategory = subCategory;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryLevel() {
        return categoryLevel;
    }

    public void setCategoryLevel(int categoryLevel) {
        this.categoryLevel = categoryLevel;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public String getIconImageUrl() {
        return iconImageUrl;
    }

    public void setIconImageUrl(String iconImageUrl) {
        this.iconImageUrl = iconImageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
