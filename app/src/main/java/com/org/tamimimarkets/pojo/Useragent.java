package com.org.tamimimarkets.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Useragent {

    @SerializedName("register")
    @Expose
    private Login register;
    @SerializedName("login")
    @Expose
    private Login login;

    public Login getRegister() {
        return register;
    }

    public void setRegister(Login register) {
        this.register = register;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

}
