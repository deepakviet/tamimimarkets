package com.org.tamimimarkets.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerId {

    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("locality")
    @Expose
    private Locality locality;
    @SerializedName("previousCustomerId")
    @Expose
    private String previousCustomerId;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("aboutMe")
    @Expose
    private String aboutMe;
    @SerializedName("profilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("address")
    @Expose
    private List<Object> address = null;
    @SerializedName("mobileVerified")
    @Expose
    private boolean mobileVerified;
    @SerializedName("emailVerified")
    @Expose
    private boolean emailVerified;
    @SerializedName("otp")
    @Expose
    private int otp;
    @SerializedName("emailToken")
    @Expose
    private double emailToken;
    @SerializedName("productsLiked")
    @Expose
    private List<Object> productsLiked = null;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("lastModified")
    @Expose
    private String lastModified;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private int __v;
    @SerializedName("useragent")
    @Expose
    private Useragent useragent;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Locality getLocality() {
        return locality;
    }

    public void setLocality(Locality locality) {
        this.locality = locality;
    }

    public String getPreviousCustomerId() {
        return previousCustomerId;
    }

    public void setPreviousCustomerId(String previousCustomerId) {
        this.previousCustomerId = previousCustomerId;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public List<Object> getAddress() {
        return address;
    }

    public void setAddress(List<Object> address) {
        this.address = address;
    }

    public boolean isMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public double getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(double emailToken) {
        this.emailToken = emailToken;
    }

    public List<Object> getProductsLiked() {
        return productsLiked;
    }

    public void setProductsLiked(List<Object> productsLiked) {
        this.productsLiked = productsLiked;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public Useragent getUseragent() {
        return useragent;
    }

    public void setUseragent(Useragent useragent) {
        this.useragent = useragent;
    }
}
