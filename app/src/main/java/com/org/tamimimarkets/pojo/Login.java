package com.org.tamimimarkets.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

    @SerializedName("isYaBrowser")
    @Expose
    private boolean isYaBrowser;
    @SerializedName("isAuthoritative")
    @Expose
    private boolean isAuthoritative;
    @SerializedName("isMobile")
    @Expose
    private boolean isMobile;
    @SerializedName("isMobileNative")
    @Expose
    private boolean isMobileNative;
    @SerializedName("isTablet")
    @Expose
    private boolean isTablet;
    @SerializedName("isiPad")
    @Expose
    private boolean isiPad;
    @SerializedName("isiPod")
    @Expose
    private boolean isiPod;
    @SerializedName("isiPhone")
    @Expose
    private boolean isiPhone;
    @SerializedName("isiPhoneNative")
    @Expose
    private boolean isiPhoneNative;
    @SerializedName("isAndroid")
    @Expose
    private boolean isAndroid;
    @SerializedName("isAndroidNative")
    @Expose
    private boolean isAndroidNative;
    @SerializedName("isBlackberry")
    @Expose
    private boolean isBlackberry;
    @SerializedName("isOpera")
    @Expose
    private boolean isOpera;
    @SerializedName("isIE")
    @Expose
    private boolean isIE;
    @SerializedName("isEdge")
    @Expose
    private boolean isEdge;
    @SerializedName("isIECompatibilityMode")
    @Expose
    private boolean isIECompatibilityMode;
    @SerializedName("isSafari")
    @Expose
    private boolean isSafari;
    @SerializedName("isFirefox")
    @Expose
    private boolean isFirefox;
    @SerializedName("isWebkit")
    @Expose
    private boolean isWebkit;
    @SerializedName("isChrome")
    @Expose
    private boolean isChrome;
    @SerializedName("isKonqueror")
    @Expose
    private boolean isKonqueror;
    @SerializedName("isOmniWeb")
    @Expose
    private boolean isOmniWeb;
    @SerializedName("isSeaMonkey")
    @Expose
    private boolean isSeaMonkey;
    @SerializedName("isFlock")
    @Expose
    private boolean isFlock;
    @SerializedName("isAmaya")
    @Expose
    private boolean isAmaya;
    @SerializedName("isPhantomJS")
    @Expose
    private boolean isPhantomJS;
    @SerializedName("isEpiphany")
    @Expose
    private boolean isEpiphany;
    @SerializedName("isDesktop")
    @Expose
    private boolean isDesktop;
    @SerializedName("isWindows")
    @Expose
    private boolean isWindows;
    @SerializedName("isLinux")
    @Expose
    private boolean isLinux;
    @SerializedName("isLinux64")
    @Expose
    private boolean isLinux64;
    @SerializedName("isMac")
    @Expose
    private boolean isMac;
    @SerializedName("isChromeOS")
    @Expose
    private boolean isChromeOS;
    @SerializedName("isBada")
    @Expose
    private boolean isBada;
    @SerializedName("isSamsung")
    @Expose
    private boolean isSamsung;
    @SerializedName("isRaspberry")
    @Expose
    private boolean isRaspberry;
    @SerializedName("isBot")
    @Expose
    private boolean isBot;
    @SerializedName("isCurl")
    @Expose
    private boolean isCurl;
    @SerializedName("isAndroidTablet")
    @Expose
    private boolean isAndroidTablet;
    @SerializedName("isWinJs")
    @Expose
    private boolean isWinJs;
    @SerializedName("isKindleFire")
    @Expose
    private boolean isKindleFire;
    @SerializedName("isSilk")
    @Expose
    private boolean isSilk;
    @SerializedName("isCaptive")
    @Expose
    private boolean isCaptive;
    @SerializedName("isSmartTV")
    @Expose
    private boolean isSmartTV;
    @SerializedName("isUC")
    @Expose
    private boolean isUC;
    @SerializedName("isFacebook")
    @Expose
    private boolean isFacebook;
    @SerializedName("isAlamoFire")
    @Expose
    private boolean isAlamoFire;
    @SerializedName("isElectron")
    @Expose
    private boolean isElectron;
    @SerializedName("silkAccelerated")
    @Expose
    private boolean silkAccelerated;
    @SerializedName("browser")
    @Expose
    private String browser;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("os")
    @Expose
    private String os;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("isWechat")
    @Expose
    private boolean isWechat;

    public boolean isIsYaBrowser() {
        return isYaBrowser;
    }

    public void setIsYaBrowser(boolean isYaBrowser) {
        this.isYaBrowser = isYaBrowser;
    }

    public boolean isIsAuthoritative() {
        return isAuthoritative;
    }

    public void setIsAuthoritative(boolean isAuthoritative) {
        this.isAuthoritative = isAuthoritative;
    }

    public boolean isIsMobile() {
        return isMobile;
    }

    public void setIsMobile(boolean isMobile) {
        this.isMobile = isMobile;
    }

    public boolean isIsMobileNative() {
        return isMobileNative;
    }

    public void setIsMobileNative(boolean isMobileNative) {
        this.isMobileNative = isMobileNative;
    }

    public boolean isIsTablet() {
        return isTablet;
    }

    public void setIsTablet(boolean isTablet) {
        this.isTablet = isTablet;
    }

    public boolean isIsiPad() {
        return isiPad;
    }

    public void setIsiPad(boolean isiPad) {
        this.isiPad = isiPad;
    }

    public boolean isIsiPod() {
        return isiPod;
    }

    public void setIsiPod(boolean isiPod) {
        this.isiPod = isiPod;
    }

    public boolean isIsiPhone() {
        return isiPhone;
    }

    public void setIsiPhone(boolean isiPhone) {
        this.isiPhone = isiPhone;
    }

    public boolean isIsiPhoneNative() {
        return isiPhoneNative;
    }

    public void setIsiPhoneNative(boolean isiPhoneNative) {
        this.isiPhoneNative = isiPhoneNative;
    }

    public boolean isIsAndroid() {
        return isAndroid;
    }

    public void setIsAndroid(boolean isAndroid) {
        this.isAndroid = isAndroid;
    }

    public boolean isIsAndroidNative() {
        return isAndroidNative;
    }

    public void setIsAndroidNative(boolean isAndroidNative) {
        this.isAndroidNative = isAndroidNative;
    }

    public boolean isIsBlackberry() {
        return isBlackberry;
    }

    public void setIsBlackberry(boolean isBlackberry) {
        this.isBlackberry = isBlackberry;
    }

    public boolean isIsOpera() {
        return isOpera;
    }

    public void setIsOpera(boolean isOpera) {
        this.isOpera = isOpera;
    }

    public boolean isIsIE() {
        return isIE;
    }

    public void setIsIE(boolean isIE) {
        this.isIE = isIE;
    }

    public boolean isIsEdge() {
        return isEdge;
    }

    public void setIsEdge(boolean isEdge) {
        this.isEdge = isEdge;
    }

    public boolean isIsIECompatibilityMode() {
        return isIECompatibilityMode;
    }

    public void setIsIECompatibilityMode(boolean isIECompatibilityMode) {
        this.isIECompatibilityMode = isIECompatibilityMode;
    }

    public boolean isIsSafari() {
        return isSafari;
    }

    public void setIsSafari(boolean isSafari) {
        this.isSafari = isSafari;
    }

    public boolean isIsFirefox() {
        return isFirefox;
    }

    public void setIsFirefox(boolean isFirefox) {
        this.isFirefox = isFirefox;
    }

    public boolean isIsWebkit() {
        return isWebkit;
    }

    public void setIsWebkit(boolean isWebkit) {
        this.isWebkit = isWebkit;
    }

    public boolean isIsChrome() {
        return isChrome;
    }

    public void setIsChrome(boolean isChrome) {
        this.isChrome = isChrome;
    }

    public boolean isIsKonqueror() {
        return isKonqueror;
    }

    public void setIsKonqueror(boolean isKonqueror) {
        this.isKonqueror = isKonqueror;
    }

    public boolean isIsOmniWeb() {
        return isOmniWeb;
    }

    public void setIsOmniWeb(boolean isOmniWeb) {
        this.isOmniWeb = isOmniWeb;
    }

    public boolean isIsSeaMonkey() {
        return isSeaMonkey;
    }

    public void setIsSeaMonkey(boolean isSeaMonkey) {
        this.isSeaMonkey = isSeaMonkey;
    }

    public boolean isIsFlock() {
        return isFlock;
    }

    public void setIsFlock(boolean isFlock) {
        this.isFlock = isFlock;
    }

    public boolean isIsAmaya() {
        return isAmaya;
    }

    public void setIsAmaya(boolean isAmaya) {
        this.isAmaya = isAmaya;
    }

    public boolean isIsPhantomJS() {
        return isPhantomJS;
    }

    public void setIsPhantomJS(boolean isPhantomJS) {
        this.isPhantomJS = isPhantomJS;
    }

    public boolean isIsEpiphany() {
        return isEpiphany;
    }

    public void setIsEpiphany(boolean isEpiphany) {
        this.isEpiphany = isEpiphany;
    }

    public boolean isIsDesktop() {
        return isDesktop;
    }

    public void setIsDesktop(boolean isDesktop) {
        this.isDesktop = isDesktop;
    }

    public boolean isIsWindows() {
        return isWindows;
    }

    public void setIsWindows(boolean isWindows) {
        this.isWindows = isWindows;
    }

    public boolean isIsLinux() {
        return isLinux;
    }

    public void setIsLinux(boolean isLinux) {
        this.isLinux = isLinux;
    }

    public boolean isIsLinux64() {
        return isLinux64;
    }

    public void setIsLinux64(boolean isLinux64) {
        this.isLinux64 = isLinux64;
    }

    public boolean isIsMac() {
        return isMac;
    }

    public void setIsMac(boolean isMac) {
        this.isMac = isMac;
    }

    public boolean isIsChromeOS() {
        return isChromeOS;
    }

    public void setIsChromeOS(boolean isChromeOS) {
        this.isChromeOS = isChromeOS;
    }

    public boolean isIsBada() {
        return isBada;
    }

    public void setIsBada(boolean isBada) {
        this.isBada = isBada;
    }

    public boolean isIsSamsung() {
        return isSamsung;
    }

    public void setIsSamsung(boolean isSamsung) {
        this.isSamsung = isSamsung;
    }

    public boolean isIsRaspberry() {
        return isRaspberry;
    }

    public void setIsRaspberry(boolean isRaspberry) {
        this.isRaspberry = isRaspberry;
    }

    public boolean isIsBot() {
        return isBot;
    }

    public void setIsBot(boolean isBot) {
        this.isBot = isBot;
    }

    public boolean isIsCurl() {
        return isCurl;
    }

    public void setIsCurl(boolean isCurl) {
        this.isCurl = isCurl;
    }

    public boolean isIsAndroidTablet() {
        return isAndroidTablet;
    }

    public void setIsAndroidTablet(boolean isAndroidTablet) {
        this.isAndroidTablet = isAndroidTablet;
    }

    public boolean isIsWinJs() {
        return isWinJs;
    }

    public void setIsWinJs(boolean isWinJs) {
        this.isWinJs = isWinJs;
    }

    public boolean isIsKindleFire() {
        return isKindleFire;
    }

    public void setIsKindleFire(boolean isKindleFire) {
        this.isKindleFire = isKindleFire;
    }

    public boolean isIsSilk() {
        return isSilk;
    }

    public void setIsSilk(boolean isSilk) {
        this.isSilk = isSilk;
    }

    public boolean isIsCaptive() {
        return isCaptive;
    }

    public void setIsCaptive(boolean isCaptive) {
        this.isCaptive = isCaptive;
    }

    public boolean isIsSmartTV() {
        return isSmartTV;
    }

    public void setIsSmartTV(boolean isSmartTV) {
        this.isSmartTV = isSmartTV;
    }

    public boolean isIsUC() {
        return isUC;
    }

    public void setIsUC(boolean isUC) {
        this.isUC = isUC;
    }

    public boolean isIsFacebook() {
        return isFacebook;
    }

    public void setIsFacebook(boolean isFacebook) {
        this.isFacebook = isFacebook;
    }

    public boolean isIsAlamoFire() {
        return isAlamoFire;
    }

    public void setIsAlamoFire(boolean isAlamoFire) {
        this.isAlamoFire = isAlamoFire;
    }

    public boolean isIsElectron() {
        return isElectron;
    }

    public void setIsElectron(boolean isElectron) {
        this.isElectron = isElectron;
    }

    public boolean isSilkAccelerated() {
        return silkAccelerated;
    }

    public void setSilkAccelerated(boolean silkAccelerated) {
        this.silkAccelerated = silkAccelerated;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isIsWechat() {
        return isWechat;
    }

    public void setIsWechat(boolean isWechat) {
        this.isWechat = isWechat;
    }

}
