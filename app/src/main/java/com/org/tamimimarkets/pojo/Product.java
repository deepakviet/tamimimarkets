package com.org.tamimimarkets.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {

    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("variants")
    @Expose
    private List<Object> variants = null;
    @SerializedName("categoryIds")
    @Expose
    private List<String> categoryIds = null;
    @SerializedName("storeCodes")
    @Expose
    private List<String> storeCodes = null;
    @SerializedName("brandId")
    @Expose
    private String brandId;
    @SerializedName("isInStock")
    @Expose
    private boolean isInStock;
    @SerializedName("discountAmount")
    @Expose
    private double discountAmount;
    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("isSoldByWeight")
    @Expose
    private boolean isSoldByWeight;
    @SerializedName("likes")
    @Expose
    private int likes;
    @SerializedName("likedByUsers")
    @Expose
    private List<String> likedByUsers = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("specification")
    @Expose
    private String specification;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("skuCode")
    @Expose
    private String skuCode;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("media")
    @Expose
    private List<Medium> media = null;
    @SerializedName("defaultImage")
    @Expose
    private String defaultImage;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private int __v;
    @SerializedName("globalAvailability")
    @Expose
    private boolean globalAvailability;
    @SerializedName("loyaltyPoints")
    @Expose
    private int loyaltyPoints;
    @SerializedName("taxAmount")
    @Expose
    private int taxAmount;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<Object> getVariants() {
        return variants;
    }

    public void setVariants(List<Object> variants) {
        this.variants = variants;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<String> getStoreCodes() {
        return storeCodes;
    }

    public void setStoreCodes(List<String> storeCodes) {
        this.storeCodes = storeCodes;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public boolean isIsInStock() {
        return isInStock;
    }

    public void setIsInStock(boolean isInStock) {
        this.isInStock = isInStock;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isIsSoldByWeight() {
        return isSoldByWeight;
    }

    public void setIsSoldByWeight(boolean isSoldByWeight) {
        this.isSoldByWeight = isSoldByWeight;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public List<String> getLikedByUsers() {
        return likedByUsers;
    }

    public void setLikedByUsers(List<String> likedByUsers) {
        this.likedByUsers = likedByUsers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<Medium> getMedia() {
        return media;
    }

    public void setMedia(List<Medium> media) {
        this.media = media;
    }

    public String getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(String defaultImage) {
        this.defaultImage = defaultImage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public boolean isGlobalAvailability() {
        return globalAvailability;
    }

    public void setGlobalAvailability(boolean globalAvailability) {
        this.globalAvailability = globalAvailability;
    }

    public int getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(int loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public int getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(int taxAmount) {
        this.taxAmount = taxAmount;
    }

}
