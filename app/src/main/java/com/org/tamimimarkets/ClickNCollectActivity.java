package com.org.tamimimarkets;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.org.tamimimarkets.databinding.ActivityClickAndCollectBinding;
import com.org.tamimimarkets.pojo.StoreListPojo;
import com.org.tamimimarkets.presenter.storelistpresenter.StoreListPresenter;
import com.org.tamimimarkets.presenter.storelistpresenter.StoreListViewInterface;
import com.org.tamimimarkets.ui.adapter.StoreListRecyclerViewAdapter;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.volley.CustomJsonRequest;

public class ClickNCollectActivity extends AppCompatActivity implements OnMapReadyCallback, StoreListViewInterface, View.OnClickListener {

    private ActivityClickAndCollectBinding clickAndCollectBinding;
    private GoogleMap map;
    private StoreListRecyclerViewAdapter storeListRecyclerViewAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        clickAndCollectBinding = DataBindingUtil.setContentView(this, R.layout.activity_click_and_collect);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        clickAndCollectBinding.frameBack.setOnClickListener(this);
    }

    public void getStoreListData() {
        StoreListPresenter storeListPresenter = new StoreListPresenter(this, this);
        storeListPresenter.getStoreListResponse(CustomJsonRequest.GET, AppConstants.STORE_LIST_URL, AppConstants.STORE_LIST_URL, null, null, true, true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(24.692567280963445, 46.6882896423339)));
        // Setting the zoom level in the map on last position  is clicked
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(Float.parseFloat("6")));

        clickAndCollectBinding.recyclerViewStoreList.setLayoutManager(new LinearLayoutManager(this));
        storeListRecyclerViewAdapter = new StoreListRecyclerViewAdapter(this);
        clickAndCollectBinding.recyclerViewStoreList.setAdapter(storeListRecyclerViewAdapter);

        getStoreListData();
    }

    private void drawMarker(LatLng point,GoogleMap map,int position, StoreListPojo storeListPojo){
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting latitude and longitude for the marker
        markerOptions.position(point);
        markerOptions.title(storeListPojo.getData().get(position).getStoreName());
        markerOptions.snippet(storeListPojo.getData().get(position).getAddress());
        // Adding marker on the Google Map
        map.addMarker(markerOptions).showInfoWindow();
    }

    @Override
    public void getStoreListResponse(StoreListPojo storeListPojo) {
        if(storeListPojo.isSuccess() && storeListPojo.getCode() == AppConstants.SUCCESS){
            for(int i = 0; i < storeListPojo.getData().size(); i++){
                double lat = Double.parseDouble(storeListPojo.getData().get(i).getLattitude());
                double lng = Double.parseDouble(storeListPojo.getData().get(i).getLongitude());
                drawMarker(new LatLng(lat,lng),map,i,storeListPojo);
            }
            storeListRecyclerViewAdapter.setData(storeListPojo.getData());
        }
    }

    @Override
    public void onError(String response) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.frameBack:
                finish();
                break;
        }
    }
}
