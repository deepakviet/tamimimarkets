package com.org.tamimimarkets.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.org.tamimimarkets.R;
import com.org.tamimimarkets.pojo.Item;
import com.org.tamimimarkets.pojo.StoreData;
import com.org.tamimimarkets.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class StoreListRecyclerViewAdapter extends RecyclerView.Adapter<StoreListRecyclerViewAdapter.StoreListViewHolder> {

    private Context context;
    private List<StoreData> storeDataList = new ArrayList<>();


    public StoreListRecyclerViewAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<StoreData> storeDataList){
        this.storeDataList = storeDataList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public StoreListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_store_list, parent, false);
        return new StoreListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StoreListViewHolder holder, int position) {
        StoreData storeData = storeDataList.get(position);

        holder.storeName.setText(storeData.getStoreName());
        holder.storeLocation.setText(storeData.getLocationName() + "\n" + storeData.getAddress() + "\n" + storeData.getCity() + "\n" + storeData.getCountry());
    }

    @Override
    public int getItemCount() {
        return storeDataList.size();
    }

    public class StoreListViewHolder extends RecyclerView.ViewHolder {

        private TextView storeName;
        private TextView storeLocation;

        public StoreListViewHolder(@NonNull View itemView) {
            super(itemView);

            storeName = itemView.findViewById(R.id.txtStoreName);
            storeLocation = itemView.findViewById(R.id.txtStoreLocation);
        }
    }
}
