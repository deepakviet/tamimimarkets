package com.org.tamimimarkets.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.org.tamimimarkets.R;
import com.org.tamimimarkets.pojo.Item;
import com.org.tamimimarkets.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class OffersPagerAdapter extends PagerAdapter {

    private final Context context;
    private List<Item> items = new ArrayList<>();

    public OffersPagerAdapter(Context context){
        this.context = context;
    }

    public void setData(List<Item> items){
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_offers_pager, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.offersImage);
        Item item = items.get(position);
        Glide.with(context).load(AppConstants.IMAGE_BASE_URL + item.getBanner_image()).into(imageView);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
