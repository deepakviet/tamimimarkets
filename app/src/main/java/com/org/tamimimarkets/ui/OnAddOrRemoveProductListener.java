package com.org.tamimimarkets.ui;

import com.org.tamimimarkets.pojo.ProductData;

public interface OnAddOrRemoveProductListener {
    void onAddOrRemoveProduct(int quantity, ProductData productData);
}
