package com.org.tamimimarkets.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.org.tamimimarkets.R;
import com.org.tamimimarkets.pojo.Item;
import com.org.tamimimarkets.ui.OnDepartmentClickListener;
import com.org.tamimimarkets.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class ShopByBrandRecyclerViewAdapter extends RecyclerView.Adapter<ShopByBrandRecyclerViewAdapter.ShopByBrandViewHolder> {

    private OnDepartmentClickListener onDepartmentClickListener;
    private Context context;
    private List<Item> items = new ArrayList<>();

    public ShopByBrandRecyclerViewAdapter(Context context, OnDepartmentClickListener onDepartmentClickListener) {
        this.context = context;
        this.onDepartmentClickListener = onDepartmentClickListener;
    }

    public void setData(List<Item> items){
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ShopByBrandViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_brand, parent, false);
        return new ShopByBrandViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopByBrandViewHolder holder, int position) {
        Item item = items.get(position);
        Glide.with(context).load(AppConstants.IMAGE_BASE_URL + item.getImageUrl()).into(holder.imageBrand);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ShopByBrandViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageBrand;

        public ShopByBrandViewHolder(@NonNull View itemView) {
            super(itemView);

            imageBrand = itemView.findViewById(R.id.imgBrand);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onDepartmentClickListener.onBrandClick(view,items.get(getAdapterPosition()));
                }
            });
        }
    }
}
