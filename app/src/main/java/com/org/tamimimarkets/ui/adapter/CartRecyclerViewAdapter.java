package com.org.tamimimarkets.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.org.tamimimarkets.R;
import com.org.tamimimarkets.pojo.Product;
import com.org.tamimimarkets.pojo.ProductData;
import com.org.tamimimarkets.pojo.ProductId;
import com.org.tamimimarkets.pojo.StoreData;
import com.org.tamimimarkets.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class CartRecyclerViewAdapter extends RecyclerView.Adapter<CartRecyclerViewAdapter.CartViewHolder> {

    private Context context;
    private List<ProductId> productIdsList = new ArrayList<>();


    public CartRecyclerViewAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<ProductId> productId){
        this.productIdsList = productId;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);
        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        Product product = productIdsList.get(position).getProduct();

        Glide.with(context).load(AppConstants.IMAGE_BASE_URL + product.getDefaultImage().replaceAll(" ","%20")).into(holder.imgProduct);
        holder.txtProductNameAndQuantity.setText(product.getTitle());

        double actualPrice = product.getPrice();
        double discount = product.getDiscountAmount();
        double discountedPrice = actualPrice - discount;

        holder.txtProductActualPrice.setText("SAR "+actualPrice);
        holder.txtProductActualPrice.setPaintFlags(holder.txtProductActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtProductCurrentPrice.setText("SAR "+discountedPrice);
        holder.txtProductSelectedQuantity.setText(product.getQuantity()+"");


    }

    @Override
    public int getItemCount() { return productIdsList.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder {


        private final ImageView imgProduct;
        private final ImageView imgAdd;
        private final ImageView imgRemove;
        private final TextView txtProductNameAndQuantity;
        private final TextView txtProductCurrentPrice;
        private final TextView txtProductActualPrice;
        private final TextView txtProductSelectedQuantity;

        public CartViewHolder(@NonNull View itemView) {
            super(itemView);

            imgProduct = itemView.findViewById(R.id.imgProduct);
            imgAdd = itemView.findViewById(R.id.imgAddItem);
            imgRemove = itemView.findViewById(R.id.imgRemoveItem);
            txtProductNameAndQuantity = itemView.findViewById(R.id.txtProductNameAndQuantity);
            txtProductCurrentPrice = itemView.findViewById(R.id.txtProductCurrentPrice);
            txtProductActualPrice = itemView.findViewById(R.id.txtProductActualPrice);
            txtProductSelectedQuantity = itemView.findViewById(R.id.txtProductSelectedQuantity);

        }
    }
}
