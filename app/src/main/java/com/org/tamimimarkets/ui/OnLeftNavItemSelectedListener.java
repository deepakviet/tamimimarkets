package com.org.tamimimarkets.ui;

import android.view.View;

import com.org.tamimimarkets.pojo.LeftNavData;

public interface OnLeftNavItemSelectedListener {
    void onLeftNavItemSelection(View view, LeftNavData leftNavData);
}
