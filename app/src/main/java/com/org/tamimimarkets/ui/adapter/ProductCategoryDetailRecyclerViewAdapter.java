package com.org.tamimimarkets.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.org.tamimimarkets.MainActivity;
import com.org.tamimimarkets.R;
import com.org.tamimimarkets.pojo.ProductData;
import com.org.tamimimarkets.ui.OnAddOrRemoveProductListener;
import com.org.tamimimarkets.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class ProductCategoryDetailRecyclerViewAdapter extends RecyclerView.Adapter<ProductCategoryDetailRecyclerViewAdapter.ProductCategoryDetailViewHolder> {

    private OnAddOrRemoveProductListener onAddOrRemoveProductListener;
    private Context context;
    private List<ProductData> items = new ArrayList<>();
    private boolean isListView = false;

    public ProductCategoryDetailRecyclerViewAdapter(Context context, boolean isListView, OnAddOrRemoveProductListener onAddOrRemoveProductListener) {
        this.context = context;
        this.isListView = isListView;
        this.onAddOrRemoveProductListener = onAddOrRemoveProductListener;
    }

    public void setData(List<ProductData> items){
        this.items = items;
        notifyDataSetChanged();
    }

//    public void setView(boolean isListView){
//        this.isListView = isListView;
//        notifyDataSetChanged();
//    }

    @NonNull
    @Override
    public ProductCategoryDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(isListView ? R.layout.item_category_detail_list : R.layout.item_category_detail_grid, parent, false);
        return new ProductCategoryDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductCategoryDetailViewHolder holder, final int position) {
            final ProductData productData = items.get(position);

            if(productData.getDefaultImage() != null)
            Glide.with(context).load(AppConstants.IMAGE_BASE_URL + productData.getDefaultImage().replaceAll(" ", "%20")).into(holder.productImage);

            if(productData.getTitle() != null)
            holder.tvProductName.setText(productData.getTitle());

            double actualPrice = productData.getPrice();
            double discount = productData.getDiscountAmount();
            double discountPercentage = (discount/actualPrice)*100;
            double discountedPrice = actualPrice - discount;

            holder.tvActualPrice.setText("SAR "+String.format("%.2f",actualPrice));
            holder.tvActualPrice.setPaintFlags(holder.tvActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvPrice.setText("SAR "+String.format("%.2f",discountedPrice));
            holder.tvdiscount.setText(String.format("%.2f",discountPercentage)+"%");

            if(productData.getQuantity() > 0){
                holder.tvQuantity.setVisibility(View.VISIBLE);
                holder.removeImageView.setVisibility(View.VISIBLE);
                holder.tvQuantity.setText(productData.getQuantity()+"");
            }


            holder.addImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleAddRemove(true, holder,productData);
                }
            });

            holder.removeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleAddRemove(false, holder,productData);
                }
            });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public int toggleAddRemove(boolean isAdded, ProductCategoryDetailViewHolder holder,ProductData productData){
        int quantity = 0;
        if(isAdded){
            quantity = Integer.parseInt(holder.tvQuantity.getText().toString());
            if(quantity <= 1) {
                holder.tvQuantity.setVisibility(View.VISIBLE);
                holder.removeImageView.setVisibility(View.VISIBLE);
            }
            quantity = quantity + 1;
            holder.tvQuantity.setText(String.valueOf(quantity));
        }else {
            quantity = Integer.parseInt(holder.tvQuantity.getText().toString());
            if(quantity > 1){
                quantity = quantity - 1;
                holder.tvQuantity.setText(String.valueOf(quantity));
            }else {
                quantity = 0;
                holder.tvQuantity.setText(String.valueOf(quantity));
                holder.tvQuantity.setVisibility(View.GONE);
                holder.removeImageView.setVisibility(View.GONE);
            }
        }
        productData.setQuantity(quantity);
        if(quantity == 0){
            MainActivity.showBadge(context,-1);
        }else if(quantity == 1) {
            MainActivity.showBadge(context,1);
        }
        onAddOrRemoveProductListener.onAddOrRemoveProduct(quantity,productData);
        return quantity;
    }

    public class ProductCategoryDetailViewHolder extends RecyclerView.ViewHolder {

        private final ImageView removeImageView;
        private final TextView tvQuantity;
        private final ImageView productImage;
        private final TextView tvProductName;
        private final TextView tvdiscount;
        private final TextView tvActualPrice;
        private final TextView tvPrice;
        private ImageView addImageView;

        public ProductCategoryDetailViewHolder(@NonNull View itemView) {
            super(itemView);

            addImageView = (ImageView)itemView.findViewById(R.id.imgPlus);
            removeImageView = (ImageView)itemView.findViewById(R.id.imgMinus);
            productImage = (ImageView)itemView.findViewById(R.id.imgProduct);
            tvQuantity  = (TextView)itemView.findViewById(R.id.tvProductQuantity);
            tvProductName  = (TextView)itemView.findViewById(R.id.txtProductQuantity);
            tvdiscount  = (TextView)itemView.findViewById(R.id.txtDiscount);
            tvActualPrice  = (TextView)itemView.findViewById(R.id.txtProductActualPrice);
            tvPrice  = (TextView)itemView.findViewById(R.id.txtProductCurrentPrice);

        }
    }
}
