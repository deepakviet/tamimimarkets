package com.org.tamimimarkets.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.org.tamimimarkets.R;
import com.org.tamimimarkets.pojo.DashboardPojo;
import com.org.tamimimarkets.pojo.Item;
import com.org.tamimimarkets.pojo.ProductPojo;
import com.org.tamimimarkets.presenter.dashboardpresenter.DashBoardPresenter;
import com.org.tamimimarkets.presenter.dashboardpresenter.DashBoardViewInterface;
import com.org.tamimimarkets.presenter.genricpresenter.GenericPresenter;
import com.org.tamimimarkets.presenter.genricpresenter.GenericViewInterface;
import com.org.tamimimarkets.ui.OnDepartmentClickListener;
import com.org.tamimimarkets.ui.adapter.AdvertisePagerAdapter;
import com.org.tamimimarkets.ui.adapter.OffersPagerAdapter;
import com.org.tamimimarkets.ui.adapter.ShopByBrandRecyclerViewAdapter;
import com.org.tamimimarkets.ui.adapter.ShopByDepartmentRecyclerViewAdapter;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONObject;

public class HomeFragment extends Fragment implements DashBoardViewInterface, OnDepartmentClickListener {


    private static final String TAG = "HomeFragment";
    private View root;
    private ViewPager viewPager;
    private ViewPager advertisePager;
    private TabLayout tabLayout;
    private TabLayout adTabLayout;
    private RecyclerView recyclerViewShopByDepartment;
    private RecyclerView recyclerViewShopByBrand;
    private ShopByDepartmentRecyclerViewAdapter shopByDepartmentRecyclerViewAdapter;
    private ShopByBrandRecyclerViewAdapter shopByBrandRecyclerViewAdapter;
    private OffersPagerAdapter offersPagerAdapter;
    private AdvertisePagerAdapter advertisePagerAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPager = root.findViewById(R.id.offersPager);
        advertisePager = root.findViewById(R.id.advertisePager);
        tabLayout = root.findViewById(R.id.tabLayout);
        adTabLayout = root.findViewById(R.id.adTabLayout);
        recyclerViewShopByDepartment = root.findViewById(R.id.recyclerViewShopByDepartment);
        recyclerViewShopByBrand = root.findViewById(R.id.recyclerViewShopByBrand);

        offersPagerAdapter = new OffersPagerAdapter(getActivity());
        advertisePagerAdapter = new AdvertisePagerAdapter(getActivity());
        viewPager.setAdapter(offersPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        advertisePager.setAdapter(advertisePagerAdapter);
        adTabLayout.setupWithViewPager(advertisePager);

        recyclerViewShopByDepartment.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerViewShopByBrand.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        shopByDepartmentRecyclerViewAdapter = new ShopByDepartmentRecyclerViewAdapter((AppCompatActivity) getActivity(),this);
        recyclerViewShopByDepartment.setAdapter(shopByDepartmentRecyclerViewAdapter);

        shopByBrandRecyclerViewAdapter = new ShopByBrandRecyclerViewAdapter(getActivity(),this);
        recyclerViewShopByBrand.setAdapter(shopByBrandRecyclerViewAdapter);

        getDashBoardData();
    }

    public void getDashBoardData(){
        DashBoardPresenter dashBoardPresenter = new DashBoardPresenter(getActivity(),this);
        dashBoardPresenter.getDashBoardResponse(CustomJsonRequest.GET, AppConstants.DASHBOARD_URL,AppConstants.DASHBOARD_URL,null,null,true,true);
    }

    @Override
    public void getDashBoardResponse(DashboardPojo dashboardPojo) {
        if(dashboardPojo.isSuccess() && dashboardPojo.getCode() == AppConstants.SUCCESS){
            for(int i = 0; i < dashboardPojo.getData().getObject().size(); i++){
                if(dashboardPojo.getData().getObject().get(i).getId() == AppConstants.SHOP_BY_DEP_ID){
                    shopByDepartmentRecyclerViewAdapter.setData(dashboardPojo.getData().getObject().get(i).getItems());
                }else if(dashboardPojo.getData().getObject().get(i).getId() == AppConstants.SHOP_BY_BRAND_ID){
                    shopByBrandRecyclerViewAdapter.setData(dashboardPojo.getData().getObject().get(i).getItems());
                }
            }

            offersPagerAdapter.setData(dashboardPojo.getData().getBanner().getItems());
            advertisePagerAdapter.setData(dashboardPojo.getData().getAdverts().getItems());
        }
    }


    @Override
    public void onError(String response) {

    }

    @Override
    public void OnDepartmentClick(View view, Item item) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.DEP_ID, item.get_id());
        bundle.putString(AppConstants.DEP_NAME,item.getCategoryName());
        bundle.putBoolean(AppConstants.IS_DEPARTMENT,true);
        Navigation.findNavController(view).navigate(R.id.action_nav_home_to_productCategoryDetailFragment,bundle);
    }

    @Override
    public void onBrandClick(View view, Item item) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.DEP_ID, item.get_id());
        bundle.putString(AppConstants.DEP_NAME,item.getName());
        bundle.putBoolean(AppConstants.IS_DEPARTMENT,false);
        Navigation.findNavController(view).navigate(R.id.action_nav_home_to_productCategoryDetailFragment,bundle);
    }

}