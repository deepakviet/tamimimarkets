package com.org.tamimimarkets.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.org.tamimimarkets.R;
import com.org.tamimimarkets.pojo.LeftNavData;
import com.org.tamimimarkets.ui.OnLeftNavItemSelectedListener;

import java.util.List;

public class LeftNavRecyclerViewAdapter extends RecyclerView.Adapter<LeftNavRecyclerViewAdapter.LeftNavViewHolder> {

    private OnLeftNavItemSelectedListener onLeftNavItemSelectedListener;
    private List<LeftNavData> leftNavDataList;
    private Context context;


    public LeftNavRecyclerViewAdapter(Context context, List<LeftNavData> leftNavData, OnLeftNavItemSelectedListener onLeftNavItemSelectedListener) {
        this.context = context;
        this.leftNavDataList = leftNavData;
        this.onLeftNavItemSelectedListener = onLeftNavItemSelectedListener;
    }


    @NonNull
    @Override
    public LeftNavViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nav_list, parent, false);
        return new LeftNavViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LeftNavViewHolder holder, int position) {
        LeftNavData leftNavData = leftNavDataList.get(position);
        if(leftNavData.getCategoryName() != null){
            holder.categoryName.setText(leftNavData.getCategoryName());
        }

    }

    @Override
    public int getItemCount() {
        return leftNavDataList.size();
    }

    public class LeftNavViewHolder extends RecyclerView.ViewHolder {
        private TextView categoryName;

        public LeftNavViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = itemView.findViewById(R.id.txtCategoryName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLeftNavItemSelectedListener.onLeftNavItemSelection(view,leftNavDataList.get(getAdapterPosition()));
                }
            });
        }
    }
}
