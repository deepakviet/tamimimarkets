package com.org.tamimimarkets.ui;

import android.view.View;

import com.org.tamimimarkets.pojo.Item;

public interface OnDepartmentClickListener {
    void OnDepartmentClick(View position, Item item);
    void onBrandClick(View position, Item item);
}
