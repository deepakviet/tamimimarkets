package com.org.tamimimarkets.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.org.tamimimarkets.R;
import com.org.tamimimarkets.pojo.Item;
import com.org.tamimimarkets.ui.OnDepartmentClickListener;
import com.org.tamimimarkets.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class ShopByDepartmentRecyclerViewAdapter extends RecyclerView.Adapter<ShopByDepartmentRecyclerViewAdapter.ShopByDepViewHolder> {

    private AppCompatActivity context;
    private List<Item> items = new ArrayList<>();
    private OnDepartmentClickListener onDepartmentClickListener;

    public ShopByDepartmentRecyclerViewAdapter(AppCompatActivity context,OnDepartmentClickListener onDepartmentClickListener) {
        this.context = context;
        this.onDepartmentClickListener = onDepartmentClickListener;
    }

    public void setData(List<Item> items){
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ShopByDepViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_department, parent, false);
        return new ShopByDepViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopByDepViewHolder holder, final int position) {
        final Item item = items.get(position);
        holder.txtDepartment.setText(item.getCategoryName().toUpperCase());
        Glide.with(context).load(AppConstants.IMAGE_BASE_URL + item.getImageUrl()).into(holder.imageDepartment);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ShopByDepViewHolder extends RecyclerView.ViewHolder {

        private TextView txtDepartment;
        private ImageView imageDepartment;

        public ShopByDepViewHolder(@NonNull View itemView) {
            super(itemView);

            imageDepartment = itemView.findViewById(R.id.imgDepartment);
            txtDepartment = itemView.findViewById(R.id.txtDepartmentName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onDepartmentClickListener.OnDepartmentClick(view,items.get(getAdapterPosition()));
                }
            });
        }
    }
}
