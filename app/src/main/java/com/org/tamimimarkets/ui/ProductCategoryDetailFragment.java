package com.org.tamimimarkets.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.org.tamimimarkets.R;
import com.org.tamimimarkets.databinding.FragmentCategoryDetailLayoutBinding;
import com.org.tamimimarkets.pojo.Product;
import com.org.tamimimarkets.pojo.ProductData;
import com.org.tamimimarkets.pojo.ProductPojo;
import com.org.tamimimarkets.presenter.genricpresenter.GenericPresenter;
import com.org.tamimimarkets.presenter.genricpresenter.GenericViewInterface;
import com.org.tamimimarkets.ui.adapter.ProductCategoryDetailRecyclerViewAdapter;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.utils.CommonMethods;
import com.org.tamimimarkets.utils.ReadWriteFromSP;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductCategoryDetailFragment extends Fragment implements View.OnClickListener, GenericViewInterface, OnAddOrRemoveProductListener {

    private FragmentCategoryDetailLayoutBinding b;
    private View rootView;
    private boolean isListView = false;
    private ProductCategoryDetailRecyclerViewAdapter productCategoryDetailRecyclerViewAdapter;
    private List<ProductData> productData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        b = DataBindingUtil.inflate(inflater,R.layout.fragment_category_detail_layout, container, false);
        rootView = b.getRoot();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        String dep_id = bundle.getString(AppConstants.DEP_ID);
        String depName = bundle.getString(AppConstants.DEP_NAME);
        boolean isDepartment = bundle.getBoolean(AppConstants.IS_DEPARTMENT);
        b.categoryDetailRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        productCategoryDetailRecyclerViewAdapter = new ProductCategoryDetailRecyclerViewAdapter(getActivity(),isListView,this);
        b.categoryDetailRecyclerView.setAdapter(productCategoryDetailRecyclerViewAdapter);
        b.txtProductCategoryName.setText(depName);

        if(dep_id != null){
            if(isDepartment)
                getDepartmentData(dep_id);
            else
                getBrandData(dep_id);
        }
        b.imgList.setOnClickListener(this);


    }

    public void getDepartmentData(String departmentId){
        String url = AppConstants.PRODUCT_BY_CATEGORY_URL + departmentId+"&userId=5f57bf01da3a513023445436";
        Log.d("PRODUCT BY CATEGORY URL",url);
        GenericPresenter genericPresenter = new GenericPresenter(getActivity(),this);
        genericPresenter.getResponse(CustomJsonRequest.GET,AppConstants.PRODUCT_BY_CATEGORY_URL,url,null,null,true,true);
    }

    public void getBrandData(String departmentId){
        String url = AppConstants.PRODUCT_BY_BRAND_URL + departmentId+"&userId=5f57bf01da3a513023445436";
        Log.d("PRODUCT BY BRAND URL",url);
        GenericPresenter genericPresenter = new GenericPresenter(getActivity(),this);
        genericPresenter.getResponse(CustomJsonRequest.GET,AppConstants.PRODUCT_BY_CATEGORY_URL,url,null,null,true,true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgList:
                isListView = !isListView;
                b.categoryDetailRecyclerView.setLayoutManager(isListView ? new LinearLayoutManager(getActivity()) : new GridLayoutManager(getActivity(),2));
                productCategoryDetailRecyclerViewAdapter = new ProductCategoryDetailRecyclerViewAdapter(getActivity(),isListView,this);
                b.categoryDetailRecyclerView.setAdapter(productCategoryDetailRecyclerViewAdapter);
                productCategoryDetailRecyclerViewAdapter.setData(productData);

                if(isListView){
                    b.imgList.setImageResource(R.drawable.grid_view);
                }else {
                    b.imgList.setImageResource(R.drawable.ic_list_view);
                }
                break;
        }
    }

    @Override
    public void getResponse(String tag, JSONObject jsonObject) {
        if(jsonObject != null){
            Gson gson = new Gson();
            if(tag.equalsIgnoreCase(AppConstants.PRODUCT_BY_CATEGORY_URL)){
                ProductPojo productPojo = gson.fromJson(jsonObject.toString(), ProductPojo.class);
                if(productPojo.isSuccess() && productPojo.getCode() == AppConstants.SUCCESS && productPojo.getMessage().equalsIgnoreCase(AppConstants.DATA_FOUND)){
                    productData = productPojo.getData();
                    productCategoryDetailRecyclerViewAdapter.setData(productData);
                    b.txtProductCount.setText(productPojo.getData().size()+" Products");
                }else {
                    b.txtProductCount.setText("0 Products");
                }
            }else if(tag.equalsIgnoreCase(AppConstants.ADD_ORDER)){
                try {
                    boolean success = jsonObject.getBoolean("success");
                    int code = jsonObject.getInt("code");
                    String message = jsonObject.getString("message");

                    JSONObject data = jsonObject.getJSONObject("data");
                    String orderNo = data.getString("orderNumber");
                    String orderId = data.getString("_id");
                    if(success && code == AppConstants.SUCCESS && (message.equalsIgnoreCase("Data has been added") || message.equalsIgnoreCase("Data has been updated"))){
                        Toast.makeText(getActivity(), "Added", Toast.LENGTH_SHORT).show();
                        ReadWriteFromSP.writeToSP(getActivity(),AppConstants.ORDER_NO,orderNo);
                        ReadWriteFromSP.writeToSP(getActivity(),AppConstants.ORDER_ID,orderId);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onError(String response) {

    }

    public void updateOrder(int quantity,ProductData productData,String customerId,String orderNo,String orderId){
        JSONObject jsonObjectMain = new JSONObject();
        JSONObject jsonObjectProductId = new JSONObject();
        JSONArray jsonArrayProductId = new JSONArray();
        try {

            jsonObjectProductId.put("product", productData.get_id());
            jsonObjectProductId.put("quantity", quantity);
            jsonObjectProductId.put("substitution", false);
            jsonArrayProductId.put(jsonObjectProductId);

            jsonObjectMain.put("orderNumber", orderNo);
            jsonObjectMain.put( "totalPrice", "");
            jsonObjectMain.put( "customerId", customerId);
            jsonObjectMain.put( "totalWeight", "");
            jsonObjectMain.put( "productId", jsonArrayProductId);
            jsonObjectMain.put( "subtotalPrice", "");
            jsonObjectMain.put( "orderId", orderId);
            jsonObjectMain.put( "totalDiscount", "");
            jsonObjectMain.put( "totalTax", "");

            Log.d("Update Order JSON", jsonObjectMain.toString());

            GenericPresenter genericPresenter = new GenericPresenter(getActivity(),this);
            genericPresenter.getResponse(CustomJsonRequest.POST,AppConstants.ADD_ORDER,AppConstants.EDIT_ORDER,jsonObjectMain,null,false,true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addOrder(int quantity,ProductData productData,String customerId){
        JSONObject jsonObjectMain = new JSONObject();
        JSONObject jsonObjectProductId = new JSONObject();
        JSONArray jsonArrayProductId = new JSONArray();
        try {

            jsonObjectProductId.put("product", productData.get_id());
            jsonObjectProductId.put("quantity", quantity);
            jsonObjectProductId.put("substitution", false);
            jsonArrayProductId.put(jsonObjectProductId);

            jsonObjectMain.put("orderNumber", "");
            jsonObjectMain.put( "totalPrice", "");
            jsonObjectMain.put( "customerId", customerId);
            jsonObjectMain.put( "totalWeight", "");
            jsonObjectMain.put( "productId", jsonArrayProductId);
            jsonObjectMain.put( "subtotalPrice", "");
            jsonObjectMain.put( "orderId", "");
            jsonObjectMain.put( "totalDiscount", "");
            jsonObjectMain.put( "totalTax", "");

            Log.d("Add Order JSON", jsonObjectMain.toString());

            GenericPresenter genericPresenter = new GenericPresenter(getActivity(),this);
            genericPresenter.getResponse(CustomJsonRequest.POST,AppConstants.ADD_ORDER,AppConstants.ADD_ORDER,jsonObjectMain,null,false,true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onAddOrRemoveProduct(int quantity,ProductData productData) {
        String customerId = CommonMethods.getUserInfo(getActivity(),AppConstants.USER_ID);
        if(ReadWriteFromSP.readStringFromSP(getActivity(),AppConstants.ORDER_NO) != null && !ReadWriteFromSP.readStringFromSP(getActivity(),AppConstants.ORDER_NO).equalsIgnoreCase("")){
            String orderNo = ReadWriteFromSP.readStringFromSP(getActivity(),AppConstants.ORDER_NO);
            String orderId = ReadWriteFromSP.readStringFromSP(getActivity(),AppConstants.ORDER_ID);
            updateOrder(quantity,productData,customerId,orderNo,orderId);
        }else {
            addOrder(quantity,productData,customerId);
        }
    }
}
