package com.org.tamimimarkets;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.org.tamimimarkets.databinding.ActivityForgetPasswordBinding;


public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityForgetPasswordBinding activityForgetPasswordBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityForgetPasswordBinding = DataBindingUtil.setContentView(this,R.layout.activity_forget_password);


        activityForgetPasswordBinding.frameBack.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.frameBack:
                finish();
                break;
        }
    }
}
