package com.org.tamimimarkets;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.JsonObject;
import com.org.tamimimarkets.databinding.ActivityCompleteOrderBinding;
import com.org.tamimimarkets.presenter.genricpresenter.GenericPresenter;
import com.org.tamimimarkets.presenter.genricpresenter.GenericViewInterface;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.utils.CommonMethods;
import com.org.tamimimarkets.utils.ReadWriteFromSP;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CompleteOrderActivity extends AppCompatActivity implements View.OnClickListener, GenericViewInterface {

    private ActivityCompleteOrderBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_complete_order);

        binding.btnBack.setOnClickListener(this);
        binding.tvCompleteOrder.setOnClickListener(this);

    }

    public void getCompleteOrder(String orderNo, String orderId,String customerId){
        JSONObject jsonObjectMain = new JSONObject();
        try {
            jsonObjectMain.put("orderNumber", orderNo);
            jsonObjectMain.put( "orderId", orderId);
            jsonObjectMain.put( "customerId", customerId);

            Log.d("Complete Order JSON", jsonObjectMain.toString());

            GenericPresenter genericPresenter = new GenericPresenter(this,this);
            genericPresenter.getResponse(CustomJsonRequest.POST, AppConstants.HANDLE_ORDER,AppConstants.HANDLE_ORDER,jsonObjectMain,null,false,true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBack:
                finish();
                break;

            case R.id.tvCompleteOrder:
                String orderNo = ReadWriteFromSP.readStringFromSP(this,AppConstants.ORDER_NO);
                String orderId = ReadWriteFromSP.readStringFromSP(this,AppConstants.ORDER_ID);
                String customerId = CommonMethods.getUserInfo(this,AppConstants.USER_ID);
                getCompleteOrder(orderNo,orderId,customerId);
                break;
        }
    }

    @Override
    public void getResponse(String tag, JSONObject jsonObject) {
        if(jsonObject != null){
            try {
                int code = jsonObject.getInt("code");
                String message = jsonObject.getString("message");
                JSONObject data = jsonObject.getJSONObject("data");
                String orderStatus = data.getString("order_status");
                if(code == AppConstants.SUCCESS  && message.equalsIgnoreCase(AppConstants.DATA_FOUND) && orderStatus.equalsIgnoreCase("PAID")){
                    Intent feedBackIntent = new Intent(this, FeedBackActivity.class);
                    startActivity(feedBackIntent);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(String response) {

    }
}
