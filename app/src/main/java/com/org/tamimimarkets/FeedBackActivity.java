package com.org.tamimimarkets;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.IntentCompat;
import androidx.databinding.DataBindingUtil;

import com.org.tamimimarkets.databinding.ActivityFeedbackBinding;
import com.org.tamimimarkets.presenter.genricpresenter.GenericPresenter;
import com.org.tamimimarkets.presenter.genricpresenter.GenericViewInterface;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.utils.CommonMethods;
import com.org.tamimimarkets.utils.ReadWriteFromSP;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class FeedBackActivity extends AppCompatActivity implements View.OnClickListener, GenericViewInterface {

    private ActivityFeedbackBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_feedback);
        String orderNo = ReadWriteFromSP.readStringFromSP(this, AppConstants.ORDER_NO);

        binding.txtNotificationMsg.setText("Your order "+orderNo+" will be picked soon and you will be notified when\n" +
                "it will be ready for delivery. Please rest assured that you are our top\n" +
                "priority and our team will select the best quality for your satisfaction.");


        binding.btnBack.setOnClickListener(this);

        binding.imgVeryPoor.setOnClickListener(this);
        binding.imgPoor.setOnClickListener(this);
        binding.imgAverage.setOnClickListener(this);
        binding.imgGood.setOnClickListener(this);
        binding.imgExcellent.setOnClickListener(this);
    }

    public void provideFeedback(int feedback){
        String orderId = ReadWriteFromSP.readStringFromSP(this,AppConstants.ORDER_ID);

        JSONObject jsonObjectMain = new JSONObject();
        try {
            jsonObjectMain.put( "orderId", orderId);
            jsonObjectMain.put( "feedback", feedback);

            Log.d("Feedback JSON", jsonObjectMain.toString());

            GenericPresenter genericPresenter = new GenericPresenter(this,this);
            genericPresenter.getResponse(CustomJsonRequest.POST, AppConstants.PROVIDE_FEEDBACK_URL,AppConstants.PROVIDE_FEEDBACK_URL,jsonObjectMain,null,false,true);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBack:
                Intent intents = new Intent(this, MainActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intents);
                finish();
                break;

            case R.id.imgVeryPoor:
                provideFeedback(1);
                break;

            case R.id.imgPoor:
                provideFeedback(2);
                break;

            case R.id.imgAverage:
                provideFeedback(3);
                break;

            case R.id.imgGood:
                provideFeedback(4);
                break;

            case R.id.imgExcellent:
                provideFeedback(5);
                break;
        }
    }

    @Override
    public void getResponse(String tag, JSONObject jsonObject) {
        if(jsonObject != null){
            try {
                int code = jsonObject.getInt("code");
                String message = jsonObject.getString("message");
                JSONObject data = jsonObject.getJSONObject("data");
                if(code == AppConstants.SUCCESS  && message.equalsIgnoreCase(AppConstants.DATA_FOUND)){
                    ReadWriteFromSP.writeToSP(this,AppConstants.ORDER_NO,"");
                    ReadWriteFromSP.writeToSP(this,AppConstants.ORDER_ID,"");
                    Intent intents = new Intent(this, MainActivity.class);
                    intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intents);
                    finish();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(String response) {

    }
}
