package com.org.tamimimarkets;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.org.tamimimarkets.pojo.RegisterPojo;
import com.org.tamimimarkets.presenter.registeraccountpresenter.RegisterPresenter;
import com.org.tamimimarkets.presenter.registeraccountpresenter.RegisterViewInterface;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.utils.CommonMethods;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterAccountActivity extends AppCompatActivity implements View.OnClickListener, RegisterViewInterface {

    private FrameLayout backButton;
    private Spinner salutationSpinner;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etMobileNo;
    private EditText etOtp;
    private EditText etPassword;
    private Button btnSignUp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        backButton = (FrameLayout)findViewById(R.id.frameBack);
        salutationSpinner = (Spinner)findViewById(R.id.spinnerSalutation);
        etFirstName = (EditText)findViewById(R.id.etFirstName);
        etLastName = (EditText)findViewById(R.id.etLastName);
        etEmail = (EditText)findViewById(R.id.etMailId);
        etMobileNo = (EditText)findViewById(R.id.etMobileNo);
        etOtp = (EditText)findViewById(R.id.etOtp);
        etPassword = (EditText)findViewById(R.id.etPassword);
        btnSignUp = (Button)findViewById(R.id.btnSignUp);

        backButton.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.array_salutation, R.layout.item_salutation);
        salutationSpinner.setAdapter(adapter);


    }

    public void requestSignUp(){
        String firstName = salutationSpinner.getSelectedItem().toString()+" "+etFirstName.getText().toString();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fname", firstName);
            jsonObject.put( "lname", etLastName.getText().toString());
            jsonObject.put( "gender", "");
            jsonObject.put( "email", etEmail.getText().toString());
            jsonObject.put( "phoneNo", etMobileNo.getText().toString());
            jsonObject.put( "password", etPassword.getText().toString());
            jsonObject.put( "aboutMe", "");
            jsonObject.put( "dob", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RegisterPresenter registerPresenter = new RegisterPresenter(this,this);
        registerPresenter.getRegisterResponse(CustomJsonRequest.POST, AppConstants.REGISTER_URL,AppConstants.REGISTER_URL,jsonObject,null,false,true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.frameBack:
                finish();
                break;
            case R.id.btnSignUp:
                if(salutationSpinner.getSelectedItemPosition() < 1){
                    Toast.makeText(this, "Please select salutation.", Toast.LENGTH_SHORT).show();
                }else if(etFirstName.getText().toString().trim().length() < 1){
                    Toast.makeText(this, "Please enter your first name.", Toast.LENGTH_SHORT).show();
                }else if(etLastName.getText().toString().trim().length() < 1){
                    Toast.makeText(this, "Please enter your last name.", Toast.LENGTH_SHORT).show();
                }else if(!CommonMethods.isValidEmail(etEmail.getText().toString())){
                    Toast.makeText(this, "Please enter an valid mail.", Toast.LENGTH_SHORT).show();
                }else if(etPassword.getText().toString().trim().length() < 1){
                    Toast.makeText(this, "Please enter an valid password.", Toast.LENGTH_SHORT).show();
                }else {
                    requestSignUp();
                }
                break;
        }
    }

    @Override
    public void getRegisterResponse(RegisterPojo registerPojo) {
        if(registerPojo.isSuccess() && registerPojo.getCode() == AppConstants.SUCCESS){
            CommonMethods.saveLoginInformation(this,registerPojo.getData().getUser());
            finish();
            Toast.makeText(this, "Thanks for registering.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(String response) {

    }
}
