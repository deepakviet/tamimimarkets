package com.org.tamimimarkets;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.org.tamimimarkets.databinding.ActivityLoginBinding;
import com.org.tamimimarkets.pojo.LoginPojo;
import com.org.tamimimarkets.presenter.loginpresenter.LoginPresenter;
import com.org.tamimimarkets.presenter.loginpresenter.LoginViewInterface;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.utils.CommonMethods;
import com.org.tamimimarkets.utils.ReadWriteFromSP;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginViewInterface {

    private FrameLayout backButton;
    private Button buttonLogin;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private CheckBox cbRememberMe;
    private ActivityLoginBinding activityLoginBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this,R.layout.activity_login);

        backButton = (FrameLayout)findViewById(R.id.frameBack);
        buttonLogin = (Button)findViewById(R.id.btnLogin);
        editTextEmail = (EditText)findViewById(R.id.etMailId);
        editTextPassword = (EditText)findViewById(R.id.etPassword);
        cbRememberMe = (CheckBox)findViewById(R.id.cbRememberMe);


        backButton.setOnClickListener(this);
        buttonLogin.setOnClickListener(this);
        activityLoginBinding.txtSignUp.setOnClickListener(this);
        activityLoginBinding.txtForgetPassword.setOnClickListener(this);


        editTextEmail.setText(ReadWriteFromSP.readStringFromSP(this,AppConstants.USER_LOGIN_EMAIL));
        editTextPassword.setText(ReadWriteFromSP.readStringFromSP(this,AppConstants.USER_PASSWORD));
        if(ReadWriteFromSP.readBooleanFromSP(this,AppConstants.REMEMBER_ME)){
            cbRememberMe.setChecked(true);
        }else {
            cbRememberMe.setChecked(false);
        }
    }

    public void requestLogin(String email, String password){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put( "password", password);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        LoginPresenter loginPresenter = new LoginPresenter(this,this);
        loginPresenter.getLoginResponse(CustomJsonRequest.POST, AppConstants.LOGIN_URL,AppConstants.LOGIN_URL,jsonObject,null,false,true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.frameBack:
                finish();
                break;
            case R.id.btnLogin:
                if(editTextEmail.getText().toString().trim().length() < 1){
                    Toast.makeText(this, "Email can't be empty.", Toast.LENGTH_SHORT).show();
                }else if(editTextPassword.getText().toString().trim().length() < 1){
                    Toast.makeText(this, "Password can't be empty.", Toast.LENGTH_SHORT).show();
                }else {
                    if(cbRememberMe.isChecked()){
                        ReadWriteFromSP.writeToSP(this,AppConstants.USER_LOGIN_EMAIL,editTextEmail.getText().toString());
                        ReadWriteFromSP.writeToSP(this,AppConstants.USER_PASSWORD,editTextPassword.getText().toString());
                        ReadWriteFromSP.writeToSP(this,AppConstants.REMEMBER_ME,true);
                    }else {
                        ReadWriteFromSP.writeToSP(this,AppConstants.USER_LOGIN_EMAIL,"");
                        ReadWriteFromSP.writeToSP(this,AppConstants.USER_PASSWORD,"");
                        ReadWriteFromSP.writeToSP(this,AppConstants.REMEMBER_ME,false);
                    }
                    requestLogin(editTextEmail.getText().toString(),editTextPassword.getText().toString());
                }
                break;
            case R.id.txtSignUp:
                Intent signUpIntent = new Intent(this,RegisterAccountActivity.class);
                startActivity(signUpIntent);
                break;

            case R.id.txtForgetPassword:
                Intent forgetPasswordIntent = new Intent(this,ForgetPasswordActivity.class);
                startActivity(forgetPasswordIntent);
                break;
        }
    }

    @Override
    public void getLoginResponse(LoginPojo loginPojo) {
        if(loginPojo.isSuccess() && loginPojo.getCode() == AppConstants.SUCCESS){
            CommonMethods.saveLoginInformation(this,loginPojo.getData().getUser());
            finish();
            Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onError(String response) {
        Toast.makeText(this, "Login error", Toast.LENGTH_SHORT).show();
    }

}
