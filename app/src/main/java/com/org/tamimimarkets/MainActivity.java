package com.org.tamimimarkets;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.org.tamimimarkets.pojo.LeftNavData;
import com.org.tamimimarkets.pojo.LeftNavPojo;
import com.org.tamimimarkets.pojo.OrderDetailsPojo;
import com.org.tamimimarkets.presenter.genricpresenter.GenericPresenter;
import com.org.tamimimarkets.presenter.genricpresenter.GenericViewInterface;
import com.org.tamimimarkets.ui.OnLeftNavItemSelectedListener;
import com.org.tamimimarkets.ui.adapter.LeftNavRecyclerViewAdapter;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.utils.CommonMethods;
import com.org.tamimimarkets.utils.ReadWriteFromSP;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import q.rorbin.badgeview.QBadgeView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GenericViewInterface, OnLeftNavItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private TextView registerTextView;
    private TextView loginTextView;
    private LinearLayout layoutLoginAndRegister;
    private LinearLayout layoutUserName;
    private TextView textUserName;
    private TextView txtLogout;
    private AlertDialog alertDialog;
    private TextView txtSelectLocation;
    private RecyclerView menuItemsRecyclerView;
    private static BottomNavigationView bottomNavigationView;
    private static QBadgeView qBadgeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        qBadgeView = new QBadgeView(this);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);


        registerTextView = navigationView.findViewById(R.id.tvRegister);
        loginTextView = navigationView.findViewById(R.id.tvLogin);
        layoutLoginAndRegister = navigationView.findViewById(R.id.layoutLoginRegister);
        layoutUserName = navigationView.findViewById(R.id.layoutUserName);
        textUserName = navigationView.findViewById(R.id.txtUserName);
        txtLogout = findViewById(R.id.txtLogout);
        txtSelectLocation = findViewById(R.id.txtSelectLocation);
        menuItemsRecyclerView = findViewById(R.id.lst_menu_items);
        menuItemsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        loginTextView.setOnClickListener(this);
        registerTextView.setOnClickListener(this);
        txtLogout.setOnClickListener(this);
        txtSelectLocation.setOnClickListener(this);

        getLeftNavData();

        if(CommonMethods.getUserInfo(this,AppConstants.USER_ID) != null)
        getOrderDetails(CommonMethods.getUserInfo(this,AppConstants.USER_ID));

    }

    public static void showBadge(Context context,int value) {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(1); // number of menu from left
        int badgeNo = qBadgeView.getBadgeNumber();
        badgeNo = badgeNo+value;
        qBadgeView.bindTarget(v).setBadgeNumber(badgeNo).setBadgeGravity(Gravity.END|Gravity.TOP).setGravityOffset(40,5,true);
    }

    public void getLeftNavData(){
        String url = AppConstants.LEFT_NAV_URL + "page=1&limit=10";
        GenericPresenter genericPresenter = new GenericPresenter(this, this);
        genericPresenter.getResponse(CustomJsonRequest.GET,AppConstants.LEFT_NAV_URL,url,null,null,true,false);
    }

    public void getOrderDetails(String userId){
        String url = AppConstants.ORDER_DETAIL_URL+userId;
        GenericPresenter genericPresenter = new GenericPresenter(this,this);
        genericPresenter.getResponse(CustomJsonRequest.GET,AppConstants.ORDER_DETAIL_URL,url,null,null,false,true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(CommonMethods.getUserInfo(this, AppConstants.USER_EMAIL) != null){
            layoutLoginAndRegister.setVisibility(View.GONE);
            layoutUserName.setVisibility(View.VISIBLE);
            txtLogout.setVisibility(View.VISIBLE);
            String firstName = CommonMethods.getUserInfo(this,AppConstants.USER_FIRST_NAME);
            String lastName = CommonMethods.getUserInfo(this,AppConstants.USER_LAST_NAME);

            textUserName.setText("Hi, "+firstName+" "+lastName);
        }else {
            layoutLoginAndRegister.setVisibility(View.VISIBLE);
            layoutUserName.setVisibility(View.GONE);
            txtLogout.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvRegister:
                Intent registerIntent = new Intent(MainActivity.this, RegisterAccountActivity.class);
                startActivity(registerIntent);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        drawer.closeDrawer(Gravity.LEFT);
                    }
                },200);
                break;

            case R.id.tvLogin:
                Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        drawer.closeDrawer(Gravity.LEFT);
                    }
                },200);
                break;

            case R.id.txtLogout:
                CommonMethods.clearLoginPref(this);
                layoutLoginAndRegister.setVisibility(View.VISIBLE);
                layoutUserName.setVisibility(View.GONE);
                txtLogout.setVisibility(View.GONE);

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        drawer.closeDrawer(Gravity.LEFT);
                    }
                },200);
                break;

            case R.id.txtSelectLocation:
                showSelectLocationDialog(view);
                break;
        }
    }

    public void showSelectLocationDialog(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        final View dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.dialog_select_location, viewGroup, false);
        Button clickNCollectButton = (Button)dialogView.findViewById(R.id.btnClickNCollect);
        Button homeDeliveryButton = (Button)dialogView.findViewById(R.id.btnHomeDelivery);

        clickNCollectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent clickAndCollectIntent = new Intent(MainActivity.this,ClickNCollectActivity.class);
                startActivity(clickAndCollectIntent);
                alertDialog.dismiss();
            }
        });

        homeDeliveryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();
            }
        });

        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.setTitle("DELIVERY TYPE");
        alertDialog.show();
    }

    @Override
    public void getResponse(String tag, JSONObject jsonObject) {
        if(jsonObject != null){
            Gson gson = new Gson();
            if(tag.equalsIgnoreCase(AppConstants.LEFT_NAV_URL)){
                LeftNavPojo leftNavPojo = gson.fromJson(jsonObject.toString(),LeftNavPojo.class);
                if(leftNavPojo.isSuccess() && leftNavPojo.getCode() == AppConstants.SUCCESS){
                    menuItemsRecyclerView.setAdapter(new LeftNavRecyclerViewAdapter(this,leftNavPojo.getData(),this));
                }
            }else if(tag.equalsIgnoreCase(AppConstants.ORDER_DETAIL_URL)){
                OrderDetailsPojo orderDetailsPojo = gson.fromJson(jsonObject.toString(),OrderDetailsPojo.class);
                String orderId = orderDetailsPojo.getData().get_id();
                String orderNo = orderDetailsPojo.getData().getOrderNumber();

                ReadWriteFromSP.writeToSP(this,AppConstants.ORDER_ID,orderId);
                ReadWriteFromSP.writeToSP(this,AppConstants.ORDER_NO,orderNo);
            }
        }
    }

    @Override
    public void onError(String response) {

    }

    @Override
    public void onLeftNavItemSelection(View view, LeftNavData leftNavData) {
        drawer.closeDrawer(Gravity.LEFT);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.DEP_ID, leftNavData.get_id());
        bundle.putString(AppConstants.DEP_NAME,leftNavData.getCategoryName());

        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        NavController navCo = navHostFragment.getNavController();
        navCo.navigate(R.id.action_nav_home_to_productCategoryDetailFragment,bundle);
    }
}