package com.org.tamimimarkets;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.org.tamimimarkets.databinding.ActivityCartBinding;
import com.org.tamimimarkets.pojo.OrderDetailsPojo;
import com.org.tamimimarkets.presenter.genricpresenter.GenericPresenter;
import com.org.tamimimarkets.presenter.genricpresenter.GenericViewInterface;
import com.org.tamimimarkets.ui.adapter.CartRecyclerViewAdapter;
import com.org.tamimimarkets.utils.AppConstants;
import com.org.tamimimarkets.utils.CommonMethods;
import com.org.tamimimarkets.volley.CustomJsonRequest;

import org.json.JSONObject;

public class CartActivity extends AppCompatActivity implements View.OnClickListener, GenericViewInterface {

    private ActivityCartBinding binding;
    private CartRecyclerViewAdapter cartRecyclerViewAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_cart);
        binding.toolbar.setContentInsetStartWithNavigation(0);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //binding.toolbar.setNavigationIcon(R.drawable.top_back);
        binding.btnBack.setOnClickListener(this);
        binding.txtCheckout.setOnClickListener(this);

        binding.recyclerViewCart.setLayoutManager(new LinearLayoutManager(this));
        cartRecyclerViewAdapter = new CartRecyclerViewAdapter(this);
        binding.recyclerViewCart.setAdapter(cartRecyclerViewAdapter);

        getCartData(CommonMethods.getUserInfo(this,AppConstants.USER_ID));

    }

    public void getCartData(String userId){
        String url = AppConstants.ORDER_DETAIL_URL+userId;
        Log.e("CART URL",url);
        GenericPresenter genericPresenter = new GenericPresenter(this,this);
        genericPresenter.getResponse(CustomJsonRequest.GET,AppConstants.ORDER_DETAIL_URL,url,null,null,false,true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBack:
                finish();
                break;

            case R.id.txtCheckout:
                Intent completeOrderIntent = new Intent(this, CompleteOrderActivity.class);
                startActivity(completeOrderIntent);
                break;
        }
    }

    @Override
    public void getResponse(String tag, JSONObject jsonObject) {
        if(jsonObject != null){
            Gson gson = new Gson();
            OrderDetailsPojo orderDetailsPojo = gson.fromJson(jsonObject.toString(),OrderDetailsPojo.class);
            if(orderDetailsPojo.isSuccess() && orderDetailsPojo.getCode() == AppConstants.SUCCESS && orderDetailsPojo.getMessage().equalsIgnoreCase(AppConstants.DATA_FOUND)){
                binding.layoutBottom.setVisibility(View.VISIBLE);
                binding.txtEmptyCart.setVisibility(View.GONE);
                binding.recyclerViewCart.setVisibility(View.VISIBLE);
                cartRecyclerViewAdapter.setData(orderDetailsPojo.getData().getProductId());
            }else {
                binding.layoutBottom.setVisibility(View.GONE);
                binding.txtEmptyCart.setVisibility(View.VISIBLE);
                binding.recyclerViewCart.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onError(String response) {

    }
}
