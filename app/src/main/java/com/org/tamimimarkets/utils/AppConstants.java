package com.org.tamimimarkets.utils;

import com.org.tamimimarkets.pojo.Banner;

public interface AppConstants {
    String SERVER_NOT_RESPOND = "server not responding";
    String NETWORK_NOT_AVAILABLE = "Network not available";
    String JSONEXPECTION = "JSONEXPECTION";

    String LOGIN_SHARED_PREFERENCE = "LoginData";

    String BASE_URL = "http://134.209.153.34:5000/v1/";
    String IMAGE_BASE_URL = "http://134.209.153.34:5000/";
    String LOGIN_URL = BASE_URL + "customer/login";
    String DASHBOARD_URL = BASE_URL + "core/fetch-landing-details";
    String REGISTER_URL = BASE_URL + "customer/register";
    String STORE_LIST_URL = BASE_URL + "core/stores";
    String PRODUCT_BY_ID_URL = BASE_URL + "/core/product-by-id?productId=";
    String PRODUCT_BY_CATEGORY_URL = BASE_URL + "core/product-by-category?categoryId=";
    String PRODUCT_BY_BRAND_URL = BASE_URL + "core/product-by-brand?brandId=";
    String LEFT_NAV_URL = BASE_URL + "core/categories?";
    String ORDER_DETAIL_URL = BASE_URL + "core/order-details?userId=";
    String ADD_ORDER = BASE_URL + "core/add-order";
    String EDIT_ORDER = BASE_URL + "core/edit-order";
    String HANDLE_ORDER = BASE_URL + "core/handle-order";
    String PROVIDE_FEEDBACK_URL = BASE_URL + "core/provide-feedback";

    String USER_FIRST_NAME = "first_name";
    String USER_LAST_NAME = "last_name";
    String USER_EMAIL = "email";
    String USER_GENDER = "gender";
    String USER_ID = "user_id";
    String IS_FIRST_TIME = "is_first_time";
    int SUCCESS = 201;
    int SHOP_BY_DEP_ID = 2;
    int SHOP_BY_BRAND_ID = 1;
    String PREFRENCE_NAME = "tamimi_markets";
    String USER_PASSWORD = "password";
    String USER_LOGIN_EMAIL = "login_email";
    String DATA_FOUND = "Data Found";

    String REMEMBER_ME = "remember_me";
    String DEP_ID = "dep_id";
    String DEP_NAME = "dep_name";
    String IS_DEPARTMENT = "is_department";
    String ORDER_ID = "order_id";
    String ORDER_NO = "order_no";
}
