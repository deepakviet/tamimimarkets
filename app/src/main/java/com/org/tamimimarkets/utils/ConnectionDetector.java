package com.org.tamimimarkets.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {

    private Context _context;

    public ConnectionDetector(Context context) {
        this._context = context;
    }

    /**
     * Checking for all possible internet providers
     **/
    public static boolean isConnectingToInternet(Context _context) {
        try {
            if (_context != null) {
                ConnectivityManager connectivity = null;
                connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivity != null) {
                    NetworkInfo[] info = connectivity.getAllNetworkInfo();
                    if (info != null)
                        for (int i = 0; i < info.length; i++)
                            if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                                return true;
                            }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
