package com.org.tamimimarkets.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Patterns;

import com.org.tamimimarkets.AppController;
import com.org.tamimimarkets.pojo.LoginPojo;
import com.org.tamimimarkets.pojo.User;

public class CommonMethods {

    public static void saveLoginInformation(Context context, User user) {
        SharedPreferences pref = context.getSharedPreferences(AppConstants.LOGIN_SHARED_PREFERENCE, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(AppConstants.USER_FIRST_NAME, user.getFname()); // Storing boolean - true/false
        editor.putString(AppConstants.USER_LAST_NAME, user.getLname()); // Storing boolean - true/false
        editor.putString(AppConstants.USER_EMAIL, user.getEmail()); // Storing string
        editor.putString(AppConstants.USER_GENDER, user.getGender()); // Storing integer
        editor.putString(AppConstants.USER_ID, user.getId()); // Storing integer
        editor.putBoolean(AppConstants.IS_FIRST_TIME, true);
        editor.commit();
    }

    public static String getUserInfo(Context context, String key) {
        if (context != null) {
            SharedPreferences pref = context.getSharedPreferences(AppConstants.LOGIN_SHARED_PREFERENCE, 0); // 0 - for private mode
            return pref.getString(key, null);
        } else {
            return null;
        }
    }

    public static void clearLoginPref(Context context) {
        SharedPreferences pref = context.getSharedPreferences(AppConstants.LOGIN_SHARED_PREFERENCE, 0); // 0 - for private mode
        pref.edit().clear().commit();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

}
