package com.org.tamimimarkets.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

public class ReadWriteFromSP {

    public static void writeToSP(Context context, String key, Object value) {
        if (context != null && !TextUtils.isEmpty(key)) {
            SharedPreferences preference = context.getSharedPreferences(AppConstants.PREFRENCE_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preference.edit();
            if (value instanceof Boolean) {
                editor.putBoolean(key, (Boolean) value);
            } else if (value instanceof String) {
                editor.putString(key, (String) value);
            } else if (value instanceof Integer) {
                editor.putInt(key, (Integer) value);
            } else if (value instanceof Long) {
                editor.putLong(key, (Long) value);
            }
            editor.commit();
        }
    }

    public static String readStringFromSP(Context context, String key) {
        if (context != null && !TextUtils.isEmpty(key)) {
            SharedPreferences preference = context.getSharedPreferences(AppConstants.PREFRENCE_NAME, Context.MODE_PRIVATE);
            return preference.getString(key, "");
        }
        return "";

    }

    public static int readIntegerFromSP(Context context, String key) {
        if (context != null && !TextUtils.isEmpty(key)) {
            SharedPreferences preference = context.getSharedPreferences(AppConstants.PREFRENCE_NAME, Context.MODE_PRIVATE);
            return preference.getInt(key, Integer.MIN_VALUE);
        }
        return Integer.MIN_VALUE;
    }

    public static long readLongFromSP(Context context, String key) {
        if (context != null && !TextUtils.isEmpty(key)) {
            SharedPreferences preference = context.getSharedPreferences(AppConstants.PREFRENCE_NAME, Context.MODE_PRIVATE);
            return preference.getLong(key, 0l);
        }
        return 0l;

    }

    public static boolean readBooleanFromSP(Context context, String key) {
        if (context != null && !TextUtils.isEmpty(key)) {
            SharedPreferences preference = context.getSharedPreferences(AppConstants.PREFRENCE_NAME, Context.MODE_PRIVATE);
            return preference.getBoolean(key, false);
        }
        return false;

    }
}
